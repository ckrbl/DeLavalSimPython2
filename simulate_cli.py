#!/usr/bin/env python3

from package.grains.Grain import Grain
from package.burner.Burn import burn
from package.fuel.Kno3Su import Kno3Su
from package.bin.simulation_parameters_pb2 import SimulationParameters

def main():
    sim_params = SimulationParameters()
    
    grain_outer_diam = input("Enter grain outer diameter (in): ")
    sim_params.grain_outer_diam_in = float(grain_outer_diam)
    grain_inner_diam = input("Enter grain inner diameter (in): ")
    sim_params.grain_inner_diam_in = float(grain_inner_diam)
    grain_length = input("Enter chamber length (in): ")
    sim_params.chamber_length_in = float(grain_length)
    throat_diam = input("Enter nozzle throat diameter (in): ")
    sim_params.throat_diam_in = float(throat_diam)
    fuel = Kno3Su()
    grain = Grain(grain_outer_diam, grain_inner_diam, grain_length)
    time = burn(grain, fuel, throat_diam)
    print("sim completed in {}".format(time))

if __name__ == '__main__':
    main()
