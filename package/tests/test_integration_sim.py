
from ..input.BurnProto import burn_proto


def test_small_nozzle():
    # Output from ./simulate_read.py 1 0.4 12 0.2
    small_nozzle_input = b'\t\x00\x00\x00\x00\x00\x00\xf0?\x11\x9a\x99\x99\x99\x99\x99\xd9?\x19\x00\x00\x00\x00\x00\x00(@!\x9a\x99\x99\x99\x99\x99\xc9?'
    sim_results = burn_proto(small_nozzle_input)
    assert (len(sim_results.results)/10 == 129/10)


def test_large_nozzle():
    # Output from ./simulate_read.py 1 0.4 12 0.3
    small_nozzle_input = b'\t\x00\x00\x00\x00\x00\x00\xf0?\x11\x9a\x99\x99\x99\x99\x99\xd9?\x19\x00\x00\x00\x00\x00\x00(@!333333\xd3?'
    sim_results = burn_proto(small_nozzle_input)
    assert (len(sim_results.results)/10 == 67/10)
