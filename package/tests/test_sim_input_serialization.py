from ..bin.simulation_parameters_pb2 import SimulationParameters
from ..input.SimulationRead import main, create_parser

def test_parser_read():
    parser = create_parser()
    args = parser.parse_args(['1.0', '0.4', '6.0', '0.2'])

    assert args.GRAIN_OUTER_DIAM == 1.0
    assert args.GRAIN_INNER_DIAM == 0.4
    assert args.CHAMBER_LENGTH == 6.0
    assert args.THROAT_DIAM == 0.2

def test_serialization():
    parser = create_parser()
    args = parser.parse_args(['1.0', '0.4', '6.0', '0.2'])
    serialized_output = main(args)
    
    sim_params = SimulationParameters()
    
    sim_params.grain_outer_diam_in = 1.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 6.0
    sim_params.throat_diam_in = 0.2
    
    print("a {}".format(serialized_output))
    print("b {}".format(sim_params.SerializeToString()))
    assert serialized_output == sim_params.SerializeToString()

def test_serialization2():
    parser = create_parser()
    args = parser.parse_args(['1.0', '0.4', '50.0', '0.2'])
    serialized_output = main(args)
    
    sim_params = SimulationParameters()
    
    sim_params.grain_outer_diam_in = 1.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 100
    sim_params.throat_diam_in = 0.2
    
    print("a {}".format(serialized_output))
    print("b {}".format(sim_params.SerializeToString()))
    assert serialized_output != sim_params.SerializeToString()

def test_serialization3():
    expected_serialization = b'\t\x00\x00\x00\x00\x00\x00\xf0?\x11\x9a\x99\x99\x99\x99\x99\xd9?\x19\x00\x00\x00\x00\x00\x00\x18@!\x9a\x99\x99\x99\x99\x99\xc9?'
    parser = create_parser()
    args = parser.parse_args(['1.0', '0.4', '6.0', '0.2'])
    serialized_output = main(args)

    assert expected_serialization == serialized_output
