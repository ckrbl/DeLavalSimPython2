#!/usr/bin/env python3

import pytest
from zmq import Context, PUSH, PULL
from threading import Event, Thread
from sys import path
from ..db.Job import Job, Base
from ..jobmanager.JobManager import JobManagerRoot, JobManagerWorkers, Worker
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from time import sleep
from pdb import set_trace
from uuid import uuid4

path.insert(0, "package/bin")

from package.bin.simulation_parameters_pb2 import SimulationParameters
from package.bin.simulation_output_pb2 import SimulationOutputs
from package.bin.jobmanager_message_pb2 import JobManagerMessage
from package.bin.jobmanager_configuration_pb2 import JobManagerConfiguration, SimulationRunner, SimulationRoot


ROOT_MANAGER_PORT = 5553
CHILD_MANAGER_PORT = 5555

engine1 = create_engine('sqlite://', connect_args={'check_same_thread': False})  # creates temporary in-memory database
Session1 = sessionmaker()
engine2 = create_engine('sqlite://', connect_args={'check_same_thread': False})  # creates temporary in-memory database
Session2 = sessionmaker()
engine3 = create_engine('sqlite://', connect_args={'check_same_thread': False})  # creates temporary in-memory database
Session3 = sessionmaker()
engine4 = create_engine('sqlite://', connect_args={'check_same_thread': False})  # creates temporary in-memory database
Session4 = sessionmaker()
# Connection to the database. One shared across all tests


@pytest.fixture(scope='module')
def connection1():
    connection = engine1.connect()
    Base.metadata.create_all(engine1)
    yield connection
    connection.close()

@pytest.fixture(scope='module')
def connection2():
    connection = engine2.connect()
    Base.metadata.create_all(engine2)
    yield connection
    connection.close()

@pytest.fixture(scope='module')
def connection3():
    connection = engine3.connect()
    Base.metadata.create_all(engine3)
    yield connection
    connection.close()

@pytest.fixture(scope='module')
def connection4():
    connection = engine4.connect()
    Base.metadata.create_all(engine4)
    yield connection
    connection.close()

# A temporary session that gets reverted in each test. One per test
@pytest.fixture(scope='function')
def session1(connection1):
    transaction = connection1.begin()
    session = Session1(bind=connection1)
    yield session
    session.close()
    transaction.rollback()

@pytest.fixture(scope='function')
def session2(connection2):
    transaction = connection2.begin()
    session = Session2(bind=connection2)
    yield session
    session.close()
    transaction.rollback()

@pytest.fixture(scope='function')
def session3(connection3):
    transaction = connection3.begin()
    session = Session3(bind=connection3)
    yield session
    session.close()
    transaction.rollback()

@pytest.fixture(scope='function')
def session4(connection4):
    transaction = connection4.begin()
    session = Session4(bind=connection4)
    yield session
    session.close()
    transaction.rollback()

def test_1_runner(session1):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2
    jmm = JobManagerMessage()
    jmm.run_simulation.sim_params.CopyFrom(sim_params)
    jmm.run_simulation.uuid = uuid4().bytes

    jm_child_config = JobManagerConfiguration()
    jm_child_config.port_start = CHILD_MANAGER_PORT
    jm_child_config.sim_runner.num_workers = 1

    j_child = JobManagerWorkers(jm_child_config, session1)
    j_child.start()
    j_child.post(jmm)
    j_child.wait_stop()

def test_10_runners(session1):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2
    jmm = JobManagerMessage()
    jmm.run_simulation.sim_params.CopyFrom(sim_params)
    jmm.run_simulation.uuid = uuid4().bytes

    jm_child_config = JobManagerConfiguration()
    jm_child_config.port_start = CHILD_MANAGER_PORT
    jm_child_config.sim_runner.num_workers = 1

    j_child = JobManagerWorkers(jm_child_config, session1)
    j_child.start()
    j_child.post(jmm)
    j_child.wait_stop()


def test_multiple_managers(session1, session2, session3, session4):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jmm = JobManagerMessage()
    jmm.run_simulation.sim_params.CopyFrom(sim_params)
    jmm.run_simulation.uuid = uuid4().bytes
    jm_child_config = JobManagerConfiguration()
    jm_child_config.port_start = CHILD_MANAGER_PORT
    jm_child_config.sim_runner.num_workers = 2
    jm_child_config.sim_runner.root_server_pull_jobs = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT)
    jm_child_config.sim_runner.root_server_push_results = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT+1)

    jm_root_config = JobManagerConfiguration()
    jm_root_config.port_start = ROOT_MANAGER_PORT
    _sr = SimulationRoot()
    jm_root_config.sim_root.CopyFrom(_sr)
    j_root = JobManagerRoot(jm_root_config, session1)
    
    j_child = JobManagerWorkers(jm_child_config, session2)
    j_child.start()

    j_root.start()
    job_id = j_root.post(jmm)
    res = j_root.get_result(job_id)
    count = 0
    while not res:
        sleep(0.1)
        res = j_root.get_result(job_id)
        count += 1
        if count > 100:
            assert False, "it took more than 10 seconds to get a result"
    j_root.stop()
    j_child.stop()


    # Verify that simulation parameters were all as expected
    jobs = session1.query(Job).all()
    assert jobs is not None and len(jobs) == 1
    j = session1.query(Job).first()
    sim_params2 = SimulationParameters()
    sim_params2.ParseFromString(j.simulation_data)
    assert sim_params2.grain_outer_diam_in == sim_params.grain_outer_diam_in
    assert sim_params2.grain_inner_diam_in == sim_params.grain_inner_diam_in
    assert sim_params2.chamber_length_in == sim_params.chamber_length_in
    assert sim_params2.throat_diam_in == sim_params.throat_diam_in

    # # Verify that simulation outputs have been computed
    sim_outputs = SimulationOutputs()
    sim_outputs.ParseFromString(j.result_data)
    assert sim_outputs.results is not None, "Simulation did not complete!"
    assert len(sim_outputs.results) > 10, "Simulation took less than 1 second!"

def test_hot_database_1_manager(session1, session2, session3, session4):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jm_child_config = JobManagerConfiguration()
    jm_child_config.port_start = CHILD_MANAGER_PORT
    jm_child_config.sim_runner.num_workers = 1

    j_child = JobManagerWorkers(jm_child_config, session1)

    job_db_object = Job(simulation_data=sim_params.SerializeToString(), uuid=uuid4().bytes)
    session1.add(job_db_object)
    session1.commit()

    j_child.start()
    j_child.wait_stop()

def test_hot_database_2_managers(session1, session2, session3, session4):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jm_child_config = JobManagerConfiguration()
    jm_child_config.port_start = CHILD_MANAGER_PORT
    jm_child_config.sim_runner.num_workers = 2
    jm_child_config.sim_runner.root_server_pull_jobs = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT)
    jm_child_config.sim_runner.root_server_push_results = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT+1)

    jm_root_config = JobManagerConfiguration()
    jm_root_config.port_start = ROOT_MANAGER_PORT
    _sr = SimulationRoot()
    jm_root_config.sim_root.CopyFrom(_sr)
    j_root = JobManagerRoot(jm_root_config, session1)
    
    j_child = JobManagerWorkers(jm_child_config, session2)

    job_db_object = Job(simulation_data=sim_params.SerializeToString(), uuid=uuid4().bytes)
    session1.add(job_db_object)
    session1.commit()
    job_id = job_db_object.uuid

    j_child.start()
    j_root.start()

    res = j_root.get_result(job_id)
    count = 0
    while not res:
        sleep(0.1)
        res = j_root.get_result(job_id)
        count += 1
        if count > 100:
            assert False, "it took more than 10 seconds to get a result"
    j_root.stop()
    j_child.stop()


    # Verify that simulation parameters were all as expected
    jobs = session1.query(Job).all()
    assert jobs is not None and len(jobs) == 1
    j = session1.query(Job).first()
    sim_params2 = SimulationParameters()
    sim_params2.ParseFromString(j.simulation_data)
    assert sim_params2.grain_outer_diam_in == sim_params.grain_outer_diam_in
    assert sim_params2.grain_inner_diam_in == sim_params.grain_inner_diam_in
    assert sim_params2.chamber_length_in == sim_params.chamber_length_in
    assert sim_params2.throat_diam_in == sim_params.throat_diam_in

    # # Verify that simulation outputs have been computed
    sim_outputs = SimulationOutputs()
    sim_outputs.ParseFromString(j.result_data)
    assert sim_outputs.results is not None, "Simulation did not complete!"
    assert len(sim_outputs.results) > 10, "Simulation took less than 1 second!"

def test_job_management_with_multiple_managers(session1, session2, session3, session4):
    NUM_JOBS = 50
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jm_child_config = JobManagerConfiguration()
    jm_child_config.port_start = CHILD_MANAGER_PORT
    jm_child_config.sim_runner.num_workers = 2
    jm_child_config.sim_runner.root_server_pull_jobs = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT)
    jm_child_config.sim_runner.root_server_push_results = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT+1)

    jm_child_config2 = JobManagerConfiguration()
    jm_child_config2.port_start = CHILD_MANAGER_PORT+2
    jm_child_config2.sim_runner.num_workers = 2
    jm_child_config2.sim_runner.root_server_pull_jobs = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT)
    jm_child_config2.sim_runner.root_server_push_results = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT+1)

    jm_child_config3 = JobManagerConfiguration()
    jm_child_config3.port_start = CHILD_MANAGER_PORT+4
    jm_child_config3.sim_runner.num_workers = 2
    jm_child_config3.sim_runner.root_server_pull_jobs = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT)
    jm_child_config3.sim_runner.root_server_push_results = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT+1)


    jm_root_config = JobManagerConfiguration()
    jm_root_config.port_start = ROOT_MANAGER_PORT
    _sr = SimulationRoot()
    jm_root_config.sim_root.CopyFrom(_sr)
    j_root = JobManagerRoot(jm_root_config, session1)
    
    j_child1 = JobManagerWorkers(jm_child_config, session2)
    j_child2 = JobManagerWorkers(jm_child_config2, session3)
    j_child3 = JobManagerWorkers(jm_child_config3, session4)

    ids_to_track = []
    for _ in range(NUM_JOBS):
        job_db_object = Job(simulation_data=sim_params.SerializeToString(), uuid=uuid4().bytes)
        session1.add(job_db_object)
        session1.commit()
        ids_to_track.append(job_db_object.uuid)

    j_child1.start()
    j_child2.start()
    j_child3.start()
    j_root.start()

    ID_TO_GET = -1
    res = j_root.get_result(ids_to_track[ID_TO_GET])
    count = 0
    while not res:
        sleep(0.1)
        res = j_root.get_result(ids_to_track[ID_TO_GET])
        count += 1
        if count > 20:
            assert False, "it took more than 10 seconds to get a result"
    j_root.stop()
    j_child1.stop()
    j_child2.stop()
    j_child3.stop()


    # Verify that simulation parameters were all as expected
    jobs = session1.query(Job).all()
    assert jobs is not None and len(jobs) == NUM_JOBS
    j = session1.query(Job).first()
    sim_params2 = SimulationParameters()
    sim_params2.ParseFromString(j.simulation_data)
    assert sim_params2.grain_outer_diam_in == sim_params.grain_outer_diam_in
    assert sim_params2.grain_inner_diam_in == sim_params.grain_inner_diam_in
    assert sim_params2.chamber_length_in == sim_params.chamber_length_in
    assert sim_params2.throat_diam_in == sim_params.throat_diam_in

    # # Verify that simulation outputs have been computed
    sim_outputs = SimulationOutputs()
    sim_outputs.ParseFromString(j.result_data)
    assert sim_outputs.results is not None, "Simulation did not complete!"
    assert len(sim_outputs.results) > 10, "Simulation took less than 1 second!"
