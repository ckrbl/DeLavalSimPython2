from ..burner.Burn import burn
from ..grains.Grain import Grain
from ..fuel.Kno3Su import Kno3Su

def compare_float(a, b):
    return abs(a-b) <= 0.01

def test_open_air_burn_1():
    grain = Grain(1, 0.4, 6)
    fuel = Kno3Su()
    sim_output =  burn(grain, fuel, None)
    time = len(sim_output.results)/10
    assert compare_float(time, 4.0), "Burn time was not what was expected!"
    
# Test that motor length with open air does not affect burn time
def test_open_air_burn_2():
    grain = Grain(1, 0.4, 2)
    fuel = Kno3Su()
    sim_output =  burn(grain, fuel, None)
    time = len(sim_output.results)/10
    assert compare_float(time, 4.0), "Burn time was not what was expected!"

# Less volume shall result in less burn time
def test_open_air_burn_3():
    grain = Grain(1, 0.6, 2)
    fuel = Kno3Su()
    sim_output =  burn(grain, fuel, None)
    time = len(sim_output.results)/10
    assert compare_float(time, 2.7), "Burn time was not what was expected!"

# Larger volume will yield substantically longer burn time (~10 seconds)
def test_open_air_burn_4():
    grain = Grain(2, 0.4, 2)
    fuel = Kno3Su()
    sim_output =  burn(grain, fuel, None)
    time = len(sim_output.results)/10
    assert compare_float(time, 10.4), "Burn time was not what was expected!"
