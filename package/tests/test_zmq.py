#!/usr/bin/env python3

import pytest
from zmq import Context, PUSH, PULL
from threading import Event, Thread
from sys import path
from ..db.Job import Job, Base
from ..jobmanager.JobManager import JobManagerWorkers, Worker
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from time import sleep
from pdb import set_trace
from uuid import uuid4

path.insert(0, "package/bin")

from package.bin.simulation_parameters_pb2 import SimulationParameters
from package.bin.simulation_output_pb2 import SimulationOutputs
from package.bin.jobmanager_message_pb2 import JobManagerMessage
from package.bin.jobmanager_configuration_pb2 import JobManagerConfiguration, SimulationRunner


MANAGER_PORT = 5555

engine = create_engine('sqlite://', connect_args={'check_same_thread': False})  # creates temporary in-memory database
Session = sessionmaker()
# Connection to the database. One shared across all tests


@pytest.fixture(scope='module')
def connection():
    connection = engine.connect()
    Base.metadata.create_all(engine)
    yield connection
    connection.close()


# A temporary session that gets reverted in each test. One per test
@pytest.fixture(scope='function')
def session(connection):
    transaction = connection.begin()
    session = Session(bind=connection)
    yield session
    session.close()
    transaction.rollback()


class JobManagerTest(object):
    def __init__(self, stop_event):
        self.stop_event = stop_event
        self.context = Context()
        self.socket = self.context.socket(PUSH)
        self.socket.bind('tcp://*:5555')
        self.socket_result = self.context.socket(PULL)
        self.socket_result.bind('tcp://*:5556')

    def run(self):
        global jm_received_message
        self.socket.send(b'Hello')
        jm_received_message = self.socket_result.recv()

        print("Received {}".format(jm_received_message))
        self.stop_event.set()


class WorkerTest(object):
    def __init__(self, stop_event):
        self.stop_event = stop_event
        self.context = Context()
        self.socket = self.context.socket(PULL)
        self.socket.connect('tcp://127.0.0.1:5555')
        self.socket_result = self.context.socket(PUSH)
        self.socket_result.connect('tcp://127.0.0.1:5556')

    def run(self, data):
        a = self.socket.recv()
        print("Received {}".format(a))
        self.socket_result.send(data)


def test_zmq_send_data():
    global jm_received_message
    EXPECTED_MSG = b'testmessage'
    stop_event = Event()
    j = JobManagerTest(stop_event)
    jt = Thread(target=j.run)
    jt.start()
    w = WorkerTest(stop_event)
    wt = Thread(target=w.run, args=(EXPECTED_MSG, ))
    wt.start()
    stop_event.wait()
    jt.join()
    wt.join()
    assert EXPECTED_MSG == jm_received_message


def test_zmq_msg_test_oneof():
    jmm = JobManagerMessage()
    jmm.test = 1
    assert jmm.HasField("test")
    assert not jmm.HasField("run_simulation")
    assert not jmm.HasField("simulation_done")


def test_zmq_send_jm_test_message():
    global jm_received_message
    EXPECTED_MSG = 1

    jmm = JobManagerMessage()
    jmm.test = EXPECTED_MSG

    stop_event = Event()
    j = JobManagerTest(stop_event)
    jt = Thread(target=j.run)
    jt.start()
    w = WorkerTest(stop_event)
    print("sending {}".format(jmm.SerializeToString()))
    wt = Thread(target=w.run, args=(jmm.SerializeToString(), ))
    wt.start()
    stop_event.wait()
    jt.join()
    wt.join()

    jmm2_bin = jm_received_message
    print("jmm2_bin {}".format(jmm2_bin))
    jmm2 = JobManagerMessage()
    jmm2.ParseFromString(jmm2_bin)
    print(jmm2)
    assert jmm2.HasField("test")
    assert EXPECTED_MSG == jmm2.test


def test_zmq_send_jm_sim_message():
    global jm_received_message

    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jmm = JobManagerMessage()
    jmm.run_simulation.sim_params.CopyFrom(sim_params)
    jmm.run_simulation.uuid = uuid4().bytes

    stop_event = Event()
    j = JobManagerTest(stop_event)
    jt = Thread(target=j.run)
    jt.start()
    w = WorkerTest(stop_event)
    print("sending {}".format(jmm.SerializeToString()))
    wt = Thread(target=w.run, args=(jmm.SerializeToString(), ))
    wt.start()
    stop_event.wait()
    jt.join()
    wt.join()

    jmm2_bin = jm_received_message
    print("jmm2_bin {}".format(jmm2_bin))
    jmm2 = JobManagerMessage()
    jmm2.ParseFromString(jmm2_bin)
    print(jmm2)
    assert jmm2.HasField("run_simulation")
    sim_params2 = jmm2.run_simulation.sim_params
    assert sim_params2.grain_outer_diam_in == sim_params.grain_outer_diam_in
    assert sim_params2.grain_inner_diam_in == sim_params.grain_inner_diam_in
    assert sim_params2.chamber_length_in == sim_params.chamber_length_in
    assert sim_params2.throat_diam_in == sim_params.throat_diam_in


def test_zmq_send_and_db_write(session):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jmm = JobManagerMessage()
    jmm.run_simulation.sim_params.CopyFrom(sim_params)
    jmm.run_simulation.uuid = uuid4().bytes
    jm_config = JobManagerConfiguration()
    jm_config.port_start = MANAGER_PORT
    jm_config.sim_runner.num_workers = 1

    j = JobManagerWorkers(jm_config, session)
    j.start()
    j.post(jmm)
    j.wait_stop()

    jobs = session.query(Job).all()
    assert jobs is not None and len(jobs) == 1
    j = jobs[0]
    sim_params2 = SimulationParameters()
    sim_params2.ParseFromString(j.simulation_data)
    assert sim_params2.grain_outer_diam_in == sim_params.grain_outer_diam_in
    assert sim_params2.grain_inner_diam_in == sim_params.grain_inner_diam_in
    assert sim_params2.chamber_length_in == sim_params.chamber_length_in
    assert sim_params2.throat_diam_in == sim_params.throat_diam_in

def test_zmq_full_get_result(session):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jmm = JobManagerMessage()
    jmm.run_simulation.sim_params.CopyFrom(sim_params)
    jmm.run_simulation.uuid = uuid4().bytes
    jm_config = JobManagerConfiguration()
    jm_config.port_start = MANAGER_PORT
    jm_config.sim_runner.num_workers = 2

    j = JobManagerWorkers(jm_config, session)
    j.start()
    j.post(jmm)
    j.wait_stop()

    # Verify that simulation parameters were all as expected
    jobs = session.query(Job).all()
    assert jobs is not None and len(jobs) == 1
    j = jobs[0]
    sim_params2 = SimulationParameters()
    sim_params2.ParseFromString(j.simulation_data)
    assert sim_params2.grain_outer_diam_in == sim_params.grain_outer_diam_in
    assert sim_params2.grain_inner_diam_in == sim_params.grain_inner_diam_in
    assert sim_params2.chamber_length_in == sim_params.chamber_length_in
    assert sim_params2.throat_diam_in == sim_params.throat_diam_in

    # Verify that simulation outputs have been computed
    sim_outputs = SimulationOutputs()
    sim_outputs.ParseFromString(j.result_data)
    assert sim_outputs.results is not None, "Simulation did not complete!"
    assert len(sim_outputs.results) > 10, "Simulation took less than 1 second!"
