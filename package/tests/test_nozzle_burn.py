from ..burner.Burn import burn
from ..grains.Grain import Grain
from ..fuel.Kno3Su import Kno3Su


def compare_float_tol(a, b, tolerance):
    return abs(a-b) <= tolerance


def compare_float(a, b):
    return abs(a-b) <= 0.01


def test_initial_pressure():
    grain = Grain(1, 0.4, 6)
    fuel = Kno3Su()
    sim_output = burn(grain, fuel, 0.2)
    assert len(sim_output.results) > 0
    assert compare_float(sim_output.results[0].chamber_pressure_psi,
                         14.6959), "Initial pressure was not what was expected!"


def test_final_pressure():
    grain = Grain(1, 0.4, 6)
    fuel = Kno3Su()
    sim_output = burn(grain, fuel, 0.2)
    # import pdb
    # pdb.set_trace()
    assert len(sim_output.results) > 0
    assert compare_float_tol(
        sim_output.results[-1].chamber_pressure_psi, 14.6959, 10), "Final pressure was not what was expected!"


def test_nozzle_burn_1():
    grain = Grain(1, 0.4, 6)
    fuel = Kno3Su()
    sim_output = burn(grain, fuel, 0.2)
    time = len(sim_output.results)/10
    assert compare_float(time, 7.1), "Burn time was not what was expected!"
