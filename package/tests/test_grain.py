#import pytest
from math import pi

from ..grains.Grain import Grain


def test_fuel_volume():
    g = Grain(2.54, 1, 15)
    assert 64.22514941366293 == g.get_fuel_volume()


def test_gap_volume():
    g = Grain(2.54, 1, 15)
    assert pow(1/2.0, 2.0) * 15 * pi == g.get_gap_volume()


def test_fuel_surface_area():
    g = Grain(2.54, 1, 15)
    assert 52.19096459482187 == g.get_burn_surface_area()
