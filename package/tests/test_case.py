#import pytest

from ..cases.Case import Case

def test_case_pressure():
    c = Case()
    assert 9928.451 == c.get_max_pressure()


def test_case_volume():
    c = Case()
    assert 76.00612186462466 == c.get_volume()
