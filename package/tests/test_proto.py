#!/usr/bin/env python3

import pytest
from sys import path

path.insert(0, "package/bin")

from package.bin.simulation_parameters_pb2 import SimulationParameters
from package.bin.simulation_output_pb2 import SimulationOutputs
from package.bin.jobmanager_message_pb2 import JobManagerMessage
from package.bin.jobmanager_configuration_pb2 import JobManagerConfiguration

def test_proto_oneof1():
    jmm = JobManagerMessage()
    jmm.test = True
    assert jmm.HasField("test")
    assert not jmm.HasField("shutdown")
    assert not jmm.HasField("run_simulation")
    assert not jmm.HasField("simulation_done")

def test_proto_oneof2():
    jmm = JobManagerMessage()
    jmm.shutdown = True
    assert not jmm.HasField("test")
    assert jmm.HasField("shutdown")
    assert not jmm.HasField("run_simulation")
    assert not jmm.HasField("simulation_done")

def test_proto_oneof3():
    jmm = JobManagerMessage()
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jmm.run_simulation.sim_params.CopyFrom(sim_params)

    assert not jmm.HasField("test")
    assert not jmm.HasField("shutdown")
    assert jmm.HasField("run_simulation")
    assert not jmm.HasField("simulation_done")

def test_proto_oneof4():
    jmm = JobManagerMessage()
    sim_outputs = SimulationOutputs()
    jmm.simulation_done.db_id = 3

    jmm.simulation_done.sim_outputs.CopyFrom(sim_outputs)

    assert not jmm.HasField("test")
    assert not jmm.HasField("shutdown")
    assert not jmm.HasField("run_simulation")
    assert jmm.HasField("simulation_done")

def test_jm_configuration():
    jmc = JobManagerConfiguration()
    jmc.sim_runner.num_workers = 3

    assert jmc.HasField("sim_runner")
    assert jmc.sim_runner.HasField("num_workers")
    assert jmc.sim_runner.num_workers == 3

def test_jm_contains_1():
    jmc = JobManagerConfiguration()
    jmc.sim_runner.num_workers = 3

    assert jmc.HasField("sim_runner")
