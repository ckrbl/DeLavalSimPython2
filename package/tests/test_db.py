#!/usr/bin/env python3

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from ..db.Job import Job, Base
from ..bin.simulation_parameters_pb2 import SimulationParameters
from ..input.BurnProto import burn_proto
import pdb

# engine = create_engine('sqlite:///test_db.sqlite')
engine = create_engine('sqlite://')  # creates temporary in-memory database
Session = sessionmaker()


# Connection to the database. One shared across all tests
@pytest.fixture(scope='module')
def connection():
    connection = engine.connect()
    Base.metadata.create_all(engine)
    yield connection
    connection.close()


# A temporary session that gets reverted in each test. One per test
@pytest.fixture(scope='function')
def session(connection):
    transaction = connection.begin()
    session = Session(bind=connection)
    yield session
    session.close()
    transaction.rollback()


def test_assign_id(session):
    j = Job(simulation_data=b'abc')
    old_id = j.id
    assert old_id == None, "Initially created job has an ID. Something is wrong"
    session.add(j)
    session.commit()
    assert j.id != old_id


def test_auto_increment_id(session):
    j1 = Job(simulation_data=b'abc')
    j2 = Job(simulation_data=b'abc')
    session.add(j1)
    session.add(j2)
    session.commit()
    assert j2.id > j1.id
    assert j1.id + 1 == j2.id


def test_get_multiple_values(session):
    j1 = Job(simulation_data=b'abc')
    j2 = Job(simulation_data=b'abc')
    session.add(j1)
    session.add(j2)
    session.commit()
    jobs = session.query(Job).all()
    assert jobs is not None and len(jobs) == 2


def test_put_with_sim_data(session):
    j1 = Job(simulation_data=b'abc')
    j2 = Job(simulation_data=b'defghi')
    session.add(j1)
    session.add(j2)
    session.commit()
    jobs = session.query(Job).filter(Job.simulation_data == b'defghi').all()
    assert jobs is not None and len(jobs) == 1


def test_delete_one(session):
    j1 = Job(simulation_data=b'abc')
    j2 = Job(simulation_data=b'defghi')
    session.add(j1)
    session.add(j2)
    session.commit()
    jobs = session.query(Job).filter(Job.simulation_data == b'defghi').delete()
    session.commit()

    jobs = session.query(Job).all()
    assert jobs is not None and len(jobs) == 1
    assert jobs[0].simulation_data == b'abc'


def test_insert_simulation(session):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    j = Job(simulation_data=sim_params.SerializeToString())
    session.add(j)
    session.commit()
    jobs = session.query(Job).all()
    assert jobs is not None and len(jobs) == 1


def test_parse_simulation(session):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    j = Job(simulation_data=sim_params.SerializeToString())
    session.add(j)
    session.commit()
    jobs = session.query(Job).all()
    assert jobs is not None and len(jobs) == 1
    j = jobs[0]
    sim_params2 = SimulationParameters()
    sim_params2.ParseFromString(j.simulation_data)

    assert sim_params.grain_outer_diam_in == sim_params2.grain_outer_diam_in
    assert sim_params.grain_inner_diam_in == sim_params2.grain_inner_diam_in
    assert sim_params.chamber_length_in == sim_params2.chamber_length_in
    assert sim_params.throat_diam_in == sim_params2.throat_diam_in


def test_read_and_run_simulation(session):
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    j = Job(simulation_data=sim_params.SerializeToString())
    session.add(j)
    session.commit()
    jobs = session.query(Job).all()
    assert jobs is not None and len(jobs) == 1
    j = jobs[0]
    sim_params2 = SimulationParameters()
    sim_params2.ParseFromString(j.simulation_data)

    assert sim_params.grain_outer_diam_in == sim_params2.grain_outer_diam_in
    assert sim_params.grain_inner_diam_in == sim_params2.grain_inner_diam_in
    assert sim_params.chamber_length_in == sim_params2.chamber_length_in
    assert sim_params.throat_diam_in == sim_params2.throat_diam_in
    sim_results = burn_proto(sim_params2.SerializeToString())
    assert sim_results
    assert len(sim_results.results) / \
        10 > 3.0, "Sim over too quick. Something is wrong!"
    assert sim_results.results[-1].chamber_pressure_psi < 25, "Simulation ended with too high pressure!"
