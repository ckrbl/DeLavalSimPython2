
# Equation for burning rocket propellant is Saint Robert's Law (a.k.a. Vieille's Law):
# r = a * p ^ n
# r = m/s
# Constants in this file were taken from

KNO3SU_A = 0.0665
KNO3SU_N = 0.319


class Kno3Su:
    def get_burn_rate(this, pressure):
        return KNO3SU_A * pow(pressure, KNO3SU_N)

    def get_density(this):
        return 1889  # kg/m3

    def get_combustion_temperature(this):
        return 1720  # degrees K

    def get_R_specific(this):
        # R_specific = R / molar_weight_combustion_products
        # R = 8.31446261815324 J*(K^-1)*(mol^-1)
        #   K = degrees Kelvin
        # Molar weight = 41.98 kg/kmol
        return 8.31446261815324/41.98

    def get_k(this):
        # Heat capacity ratio: https://en.wikipedia.org/wiki/Heat_capacity_ratio
        # This can be estimated between 1.1-1.4 for typical combustion products.
        return 1.133
