from math import pi

class Grain:
    def __init__(this, outer_diameter, inner_diameter, length):
        this.od = outer_diameter
        this.id = inner_diameter
        this.l = length
    
    def get_fuel_volume(this):
        fuel_volume =  pow(this.od/2.0, 2.0) * this.l * pi
        gap_volume = pow(this.id/2.0, 2.0) * this.l * pi
        return max(fuel_volume - gap_volume, 0)

    def get_fuel_mass(this, density):
        if this.id >= this.od:
            return 0
        vol = this.get_fuel_volume() # units = in3
        vol_m = vol * pow(0.0254, 3.0) # convert to m3
        return vol_m * density

    def get_gap_volume(this):
        gap_volume = pow(this.id/2.0, 2.0) * this.l * pi
        return gap_volume # in3

    def get_burn_surface_area(this): # in2
        # Walls of the cylinder
        sa_walls = 2.0 * pi * (this.id/2.0) * this.l
        # Rear face
        sa_rear_face = pow((this.od/2.0), 2.0) * pi 
        return sa_walls + sa_rear_face
    
    def step(this, length_burnt, density):
        # estimate mass burnt with surface area * length
        if this.get_fuel_mass(density) <= 0:
            return 0
        burn_sa = this.get_burn_surface_area()
        vol_burnt = burn_sa * length_burnt # units = in3
        vol_burnt_m = vol_burnt * pow(0.0254, 3.0) # convert to m3
        mass_burnt = density * vol_burnt_m
        this.id += length_burnt # in + in
        return mass_burnt # kg



