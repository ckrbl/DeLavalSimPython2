#!/usr/bin/env python3

import cherrypy
from cherrypy.lib import file_generator
from cherrypy.lib.static import serve_file
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from binascii import hexlify, unhexlify
from ..bin.simulation_parameters_pb2 import SimulationParameters
import sqlite3
from uuid import uuid4
from re import sub
from os.path import join, dirname, abspath
from ..db.Job import Job, Base
from pdb import set_trace
from time import time

RELEASE_WEBSERVER_FILES_DIR = join(abspath(dirname(__file__)), '..', '..', 'result-browser', 'build')

class App:
    def __init__(self, db_path):
        engine1 = create_engine(
            'sqlite:///{}'.format(db_path), connect_args={'check_same_thread': False})
        Session1 = sessionmaker(bind=engine1)
        conn1 = engine1.connect()
        Base.metadata.create_all(engine1)
        conn1.begin()
        self.db_root = Session1()

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def listsimulations(self, page = None):
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        jobs = []
        try:
            if page is not None:
                page_stripped = page.strip()
                page_stripped = sub("[^0-9]", "", page_stripped)
                if page != page_stripped:
                    # If anything had to be removed then this is bad param, 404 out
                    cherrypy.response.status = 500
                    return ""
                page = page_stripped
                jobs = self.db_root.query(Job).order_by(Job.id).limit(10).offset(page).all()
            else:
                jobs = self.db_root.query(Job).order_by(Job.id.desc()).limit(10).all()
            cherrypy.response.headers['Content-Type'] = 'application/octet-stream'
            cherrypy.response.status = 200
            output = []
            for j in jobs:
                a = {"id": hexlify(j.uuid), "date":j.date_submitted}
                output.append(a)
            return output
        except Exception as e:
            cherrypy.response.status = 500
            return str(e)

    @cherrypy.expose
    def getresult(self, param):
        cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
        try:
            param_stripped = param.strip()
            param_stripped = param_stripped.lower()
            param_stripped = sub("[^0-9a-f]", "", param_stripped)
            if param != param_stripped:
                # If anything had to be removed then this is bad param, 404 out
                cherrypy.response.status = 404
                return ""
            if len(param) != 32:
                cherrypy.response.status = 404
                return ""
            param = unhexlify(param)
            jobs = self.db_root.query(Job).filter(Job.uuid == param).all()
            if len(jobs) == 0:
                cherrypy.response.status = 404
                return ""
            else:
                cherrypy.response.headers['Content-Type'] = 'application/octet-stream'
                return jobs[0].result_data
        except Exception as e:
            cherrypy.response.status = 500
            return ""

    @cherrypy.expose
    @cherrypy.tools.allow(methods=['POST'])
    def submitnewsimulation(self, grainOuterDiameter, grainInnerDiameter, chamberLength, throatDiam):
        new_uuid = uuid4().bytes
        try:
            grainOuterDiameter = float(grainOuterDiameter)
            grainInnerDiameter = float(grainInnerDiameter)
            chamberLength = float(chamberLength)
            throatDiam = float(throatDiam)
            if not isinstance(grainOuterDiameter, float):
                raise
            if not isinstance(grainInnerDiameter, float):
                raise
            if not isinstance(chamberLength, float):
                raise
            if not isinstance(throatDiam, float):
                raise
            sim_params = SimulationParameters()
            sim_params.grain_outer_diam_in = grainOuterDiameter
            sim_params.grain_inner_diam_in = grainInnerDiameter
            sim_params.chamber_length_in = chamberLength
            sim_params.throat_diam_in = throatDiam

            j = Job(simulation_data=sim_params.SerializeToString(),
                    uuid=new_uuid, date_submitted=time())
            self.db_root.add(j)
            self.db_root.commit()
        except Exception as e:
            cherrypy.response.status = 500
            return ""
        raise cherrypy.HTTPRedirect("http://capstone:3000/dashboard/{0}".format(hexlify(new_uuid).decode('utf-8')))


    @cherrypy.expose
    @cherrypy.tools.allow(methods=['GET'])
    def newsimulation(self):
        return "This is the get"

    @cherrypy.expose
    def testroute(self):
        return "This is the test route"

    # @cherrypy.expose
    # def index(self):
    #     return "This is the index"

# class Root(object): pass

def main(db_path):
    conf = {
        '/': {
                'tools.staticdir.on': True,
                'tools.staticdir.dir': RELEASE_WEBSERVER_FILES_DIR,
                'tools.staticdir.index': 'index.html',
        }
    }
    cherrypy.server.socket_host = '0.0.0.0'
    print("Here 1" + RELEASE_WEBSERVER_FILES_DIR)
    cherrypy.quickstart(App(db_path), '/', config=conf)


if __name__ == "__main__":
    DB_FILE_PATH = abspath(join(dirname(__file__), "..", "..", "sqlite3.db"))
    main(DB_FILE_PATH)
