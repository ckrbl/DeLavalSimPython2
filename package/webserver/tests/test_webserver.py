#!/usr/bin/env python3

import requests
from pdb import set_trace

def test_webserver_running1():
    r = requests.get('http://127.0.0.1:8080')
    assert not 'test' in r.text

def test_webserver_running2():
    r = requests.get('http://127.0.0.1:8080')
    assert r.status_code == 200

def test_cherrypy_route1():
    r = requests.get('http://127.0.0.1:8080/testroute')
    assert 'index' not in r.text.lower()

def test_cherrypy_route2():
    r = requests.get('http://127.0.0.1:8080/testroute')
    assert 'test route' in r.text.lower()

def test_invalid_uuid():
    r = requests.get('http://127.0.0.1:8080/getresult?param=123')
    assert r.status_code == 404


def test_valid_but_not_found_uuid():
    r = requests.get(
        'http://127.0.0.1:8080/getresult?param=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
    assert r.status_code == 404


def test_valid_uuid_invalid_characters_stripped():
    r = requests.get(
        'http://127.0.0.1:8080/getresult?param=acc5d221e4a847dd9c04864bafc9832b]')
    assert r.status_code == 404


def test_valid_uuid():
    r = requests.get(
        'http://127.0.0.1:8080/getresult?param=a0d3c56ea23c424db267a3d79adeb2df')
    assert r.status_code == 200


def test_invalid_od():
    r = requests.post('http://127.0.0.1:8080/submitnewsimulation',
                      data={'grainInnerDiameter': 'aaa', 'grainOuterDiameter': '2.0', 'chamberLength': '5.0', 'throatDiam': '5.0'})
    assert r.status_code == 500


def test_invalid_id():
    r = requests.post('http://127.0.0.1:8080/submitnewsimulation',
                      data={'grainInnerDiameter': '1.0', 'grainOuterDiameter': 'aaa', 'chamberLength': '5.0', 'throatDiam': '5.0'})
    assert r.status_code == 500


def test_invalid_cl():
    r = requests.post('http://127.0.0.1:8080/submitnewsimulation',
                      data={'grainInnerDiameter': '1.0', 'grainOuterDiameter': '2.0', 'chamberLength': 'aaa', 'throatDiam': '5.0'})
    assert r.status_code == 500


def test_invalid_td():
    r = requests.post('http://127.0.0.1:8080/submitnewsimulation',
                      data={'grainInnerDiameter': '1.0', 'grainOuterDiameter': '2.0', 'chamberLength': '5.0', 'throatDiam': 'aaa'})
    assert r.status_code == 500


def test_valid_post():
    r = requests.post('http://127.0.0.1:8080/submitnewsimulation',
                      data={'grainInnerDiameter': '1.0', 'grainOuterDiameter': '2.0', 'chamberLength': '5.0', 'throatDiam': '0.4'})
    assert r.status_code == 200

def test_list_simulations():
    r = requests.get('http://127.0.0.1:8080/listsimulations')
    assert r.status_code == 200

def test_list_simulations_bad_page():
    r = requests.get('http://127.0.0.1:8080/listsimulations?page=a')
    assert r.status_code == 500

def test_list_simulations_valid_page():
    r = requests.get('http://127.0.0.1:8080/listsimulations?page=1')
    assert r.status_code == 200
