#import abc
from math import pi

class Case:
#    __metaclass__ = abc.ABCMeta

#    @abc.abstractmethod
    def get_volume(this):
        return pow(this._get_diameter()/2.0, 2.0)*this._get_height()*pi

    def get_max_pressure(this):
        return 9928.451

    def _get_diameter(this):
        return 2.54

    def _get_height(this):
        return 15.0
