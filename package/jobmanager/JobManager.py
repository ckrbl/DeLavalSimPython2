#!/usr/bin/env python3

from zmq import Context, PUSH, PULL, select as zselect
from threading import Thread, Event
from time import sleep
from sys import path, stdout
from ..db.Job import Job
from pdb import set_trace
from ..input.BurnProto import burn_proto
from select import select

path.insert(0, "package/bin")
from package.bin.simulation_parameters_pb2 import SimulationParameters
from package.bin.simulation_output_pb2 import SimulationOutputs
from package.bin.jobmanager_message_pb2 import JobManagerMessage
from package.bin.jobmanager_configuration_pb2 import JobManagerConfiguration, SimulationRunner


class JobManager(object):
    def __init__(self, configuration, db):
        self.configuration = self._parse_config_argument(configuration)
        self.port_start = self.configuration.port_start
        self.db = db
        # Initialize self, open ports to send and receive jobs.
        self.context = Context()
        self.stop_event = Event()
        self.push_workers_port = self.port_start
        self.pull_res_port = self.port_start + 1
        self.submitted_jobs = []

        self.socket_workers = self.context.socket(PUSH)
        self.socket_workers.bind('tcp://*:{}'.format(self.push_workers_port))
        self.socket_result = self.context.socket(PULL)
        self.socket_result.bind('tcp://*:{}'.format(self.pull_res_port))
        self.socket_wait_list = [self.socket_result]

    def _parse_config_argument(self, config_arg):
        if isinstance(config_arg, JobManagerConfiguration):
            return config_arg
        jm_configuration = JobManagerConfiguration()
        jm_configuration.ParseFromString(config_arg)
        return jm_configuration

    def start(self):
        self.jt = Thread(target=self._run)
        self.jt.start()

    def stop(self):
        self.stop_event.set()
        self.jt.join()

    def wait_stop(self):
        self.stop_event.wait()
        self.jt.join()

    def _send_job_to_workers(self, jm_message):
        self.socket_workers.send(jm_message.SerializeToString())
        self.submitted_jobs.append(jm_message.run_simulation.uuid)

    def post(self, jm_message):
        job_db_object = Job(
            simulation_data=jm_message.run_simulation.sim_params.SerializeToString(), uuid=jm_message.run_simulation.uuid)
        self.db.add(job_db_object)
        self.db.commit()
        jm_message.run_simulation.db_id = job_db_object.id
        self._send_job_to_workers(jm_message)
        return job_db_object.uuid

    def _check_database_for_jobs(self):
        jobs = self.db.query(Job).filter(Job.result_data == None).all()
        jobs = [j for j in jobs if j.uuid not in self.submitted_jobs]
        for job in jobs:
            jmm = JobManagerMessage()
            sim_params = SimulationParameters()
            sim_params.ParseFromString(job.simulation_data)
            jmm.run_simulation.sim_params.CopyFrom(sim_params)
            jmm.run_simulation.db_id = job.id
            jmm.run_simulation.uuid = job.uuid
            stdout.write("Sending job to worker-managers\n")
            self._send_job_to_workers(jmm)


class JobManagerRoot(JobManager):
    def __init__(self, configuration, db):
        JobManager.__init__(self, configuration, db)
        self.configuration = self.configuration.sim_root

    def get_result(self, uuid):
        j1 = self.db.query(Job).filter(Job.uuid == uuid).first()
        return j1.result_data

    def _process_job_result(self):
        job_result_msg = self.socket_result.recv()
        job_msg = JobManagerMessage()
        job_msg.ParseFromString(job_result_msg)
        uuid = job_msg.simulation_done.uuid
        sim_outputs = job_msg.simulation_done.sim_outputs
        j1 = self.db.query(Job).filter(Job.uuid == uuid).first()
        j1.result_data = sim_outputs.SerializeToString()
        self.db.commit()

    def _run(self):
        while True:
            r_ready, _, _ = zselect(self.socket_wait_list, [], [], 0.2)
            if self.socket_result in r_ready:
                stdout.write("RM received job result\n")
                self._process_job_result()
            if self.stop_event.is_set():
                break
            self._check_database_for_jobs()


class JobManagerWorkers(JobManager):
    def __init__(self, configuration, db):
        JobManager.__init__(self, configuration, db)
        self.configuration = self.configuration.sim_runner
        self.workers = []

        self.socket_root_pull_jobs = None
        self.socket_root_push_results = None

        if self.configuration.HasField("root_server_pull_jobs"):
            self.socket_root_pull_jobs = self.context.socket(PULL)
            self.socket_root_pull_jobs.connect(
                self.configuration.root_server_pull_jobs)
            self.socket_wait_list.append(self.socket_root_pull_jobs)
        if self.configuration.HasField("root_server_push_results"):
            self.socket_root_push_results = self.context.socket(PUSH)
            self.socket_root_push_results.connect(
                self.configuration.root_server_push_results)

        # Initialize workers
        for _ in range(self.configuration.num_workers):
            jobs_pull_url = 'tcp://127.0.0.1:{}'.format(self.push_workers_port)
            results_push_url = 'tcp://127.0.0.1:{}'.format(self.pull_res_port)
            w = Worker(jobs_pull_url, results_push_url)
            w.start()
            self.workers.append(w)

    def _shutdown_workers(self):
        for w in self.workers:
            shutdown_msg = JobManagerMessage()
            shutdown_msg.shutdown = True
            self.socket_workers.send(shutdown_msg.SerializeToString())
        for w in self.workers:
            w.stop()

    def _process_job_result(self):
        job_result_msg = self.socket_result.recv()
        job_msg = JobManagerMessage()
        job_msg.ParseFromString(job_result_msg)
        uuid = job_msg.simulation_done.uuid
        sim_outputs = job_msg.simulation_done.sim_outputs
        j1 = self.db.query(Job).filter(Job.uuid == uuid).first()
        j1.result_data = sim_outputs.SerializeToString()
        self.db.commit()
        # if no upstream, this is probably a test. quit out.
        if not self.socket_root_push_results:
            self.stop_event.set()
            return
        self.socket_root_push_results.send(job_result_msg)

    def _process_new_job(self):
        stdout.write("Worker received new job\n")
        job_req_msg = self.socket_root_pull_jobs.recv()
        job_msg = JobManagerMessage()
        job_msg.ParseFromString(job_req_msg)
        self.post(job_msg)

    def _close_sockets(self):
        self.socket_workers.close()
        self.socket_result.close()
        if self.socket_root_pull_jobs:
            self.socket_root_pull_jobs.close()
        if self.socket_root_push_results:
            self.socket_root_push_results.close()

    def _run(self):
        while True:
            r_ready, _, _ = zselect(self.socket_wait_list, [], [], 0.2)
            if self.socket_result in r_ready:
                self._process_job_result()
            if self.socket_root_pull_jobs and self.socket_root_pull_jobs in r_ready:
                self._process_new_job()
            if self.stop_event.is_set():
                self._shutdown_workers()
                self._close_sockets()
                break
            self._check_database_for_jobs()


class Worker(object):
    def __init__(self, jobs_pull_url, results_push_url):
        self.context = Context()
        self.socket = self.context.socket(PULL)
        self.socket.connect(jobs_pull_url)
        self.socket_result = self.context.socket(PUSH)
        self.socket_result.connect(results_push_url)

    def _run_job(self, j):
        params = j.sim_params
        db_id = j.db_id
        uuid = j.uuid
        results = burn_proto(params.SerializeToString())
        job_done_msg = JobManagerMessage()
        job_done_msg.simulation_done.sim_outputs.CopyFrom(results)
        job_done_msg.simulation_done.db_id = db_id
        job_done_msg.simulation_done.uuid = uuid
        self.socket_result.send(job_done_msg.SerializeToString())

    def start(self):
        self.jt = Thread(target=self.run)
        self.jt.start()

    def stop(self):
        self.jt.join()

    def run(self):
        while True:
            msg_bin = self.socket.recv()
            job_msg = JobManagerMessage()
            job_msg.ParseFromString(msg_bin)
            if job_msg.HasField("test"):
                # This is only a test, send it right back
                self.socket_result.send(msg_bin)
            elif job_msg.HasField("shutdown"):
                self.socket.close()
                self.socket_result.close()
                break
            elif job_msg.HasField("run_simulation"):
                self._run_job(job_msg.run_simulation)
