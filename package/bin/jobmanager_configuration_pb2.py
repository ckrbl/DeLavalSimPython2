# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: jobmanager_configuration.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='jobmanager_configuration.proto',
  package='delavalsim',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1ejobmanager_configuration.proto\x12\ndelavalsim\"h\n\x10SimulationRunner\x12\x13\n\x0bnum_workers\x18\x01 \x02(\x05\x12\x1d\n\x15root_server_pull_jobs\x18\x02 \x01(\t\x12 \n\x18root_server_push_results\x18\x03 \x01(\t\"\x10\n\x0eSimulationRoot\"\x9b\x01\n\x17JobManagerConfiguration\x12\x12\n\nport_start\x18\x01 \x02(\x05\x12\x32\n\nsim_runner\x18\x02 \x01(\x0b\x32\x1c.delavalsim.SimulationRunnerH\x00\x12.\n\x08sim_root\x18\x03 \x01(\x0b\x32\x1a.delavalsim.SimulationRootH\x00\x42\x08\n\x06\x63\x61reer')
)




_SIMULATIONRUNNER = _descriptor.Descriptor(
  name='SimulationRunner',
  full_name='delavalsim.SimulationRunner',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='num_workers', full_name='delavalsim.SimulationRunner.num_workers', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='root_server_pull_jobs', full_name='delavalsim.SimulationRunner.root_server_pull_jobs', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='root_server_push_results', full_name='delavalsim.SimulationRunner.root_server_push_results', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=46,
  serialized_end=150,
)


_SIMULATIONROOT = _descriptor.Descriptor(
  name='SimulationRoot',
  full_name='delavalsim.SimulationRoot',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=152,
  serialized_end=168,
)


_JOBMANAGERCONFIGURATION = _descriptor.Descriptor(
  name='JobManagerConfiguration',
  full_name='delavalsim.JobManagerConfiguration',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='port_start', full_name='delavalsim.JobManagerConfiguration.port_start', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='sim_runner', full_name='delavalsim.JobManagerConfiguration.sim_runner', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='sim_root', full_name='delavalsim.JobManagerConfiguration.sim_root', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='career', full_name='delavalsim.JobManagerConfiguration.career',
      index=0, containing_type=None, fields=[]),
  ],
  serialized_start=171,
  serialized_end=326,
)

_JOBMANAGERCONFIGURATION.fields_by_name['sim_runner'].message_type = _SIMULATIONRUNNER
_JOBMANAGERCONFIGURATION.fields_by_name['sim_root'].message_type = _SIMULATIONROOT
_JOBMANAGERCONFIGURATION.oneofs_by_name['career'].fields.append(
  _JOBMANAGERCONFIGURATION.fields_by_name['sim_runner'])
_JOBMANAGERCONFIGURATION.fields_by_name['sim_runner'].containing_oneof = _JOBMANAGERCONFIGURATION.oneofs_by_name['career']
_JOBMANAGERCONFIGURATION.oneofs_by_name['career'].fields.append(
  _JOBMANAGERCONFIGURATION.fields_by_name['sim_root'])
_JOBMANAGERCONFIGURATION.fields_by_name['sim_root'].containing_oneof = _JOBMANAGERCONFIGURATION.oneofs_by_name['career']
DESCRIPTOR.message_types_by_name['SimulationRunner'] = _SIMULATIONRUNNER
DESCRIPTOR.message_types_by_name['SimulationRoot'] = _SIMULATIONROOT
DESCRIPTOR.message_types_by_name['JobManagerConfiguration'] = _JOBMANAGERCONFIGURATION
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SimulationRunner = _reflection.GeneratedProtocolMessageType('SimulationRunner', (_message.Message,), dict(
  DESCRIPTOR = _SIMULATIONRUNNER,
  __module__ = 'jobmanager_configuration_pb2'
  # @@protoc_insertion_point(class_scope:delavalsim.SimulationRunner)
  ))
_sym_db.RegisterMessage(SimulationRunner)

SimulationRoot = _reflection.GeneratedProtocolMessageType('SimulationRoot', (_message.Message,), dict(
  DESCRIPTOR = _SIMULATIONROOT,
  __module__ = 'jobmanager_configuration_pb2'
  # @@protoc_insertion_point(class_scope:delavalsim.SimulationRoot)
  ))
_sym_db.RegisterMessage(SimulationRoot)

JobManagerConfiguration = _reflection.GeneratedProtocolMessageType('JobManagerConfiguration', (_message.Message,), dict(
  DESCRIPTOR = _JOBMANAGERCONFIGURATION,
  __module__ = 'jobmanager_configuration_pb2'
  # @@protoc_insertion_point(class_scope:delavalsim.JobManagerConfiguration)
  ))
_sym_db.RegisterMessage(JobManagerConfiguration)


# @@protoc_insertion_point(module_scope)
