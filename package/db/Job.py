#!/usr/bin/env python3

from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, BLOB
from sqlalchemy.ext.declarative import declarative_base
from ..bin.simulation_parameters_pb2 import SimulationParameters

Base = declarative_base()


class Job(Base):
    __tablename__ = 'job'
    id = Column(Integer, primary_key=True)  # Auto increments, also has database-local meaning
    uuid = Column(BLOB) # global meaning
    date_submitted = Column(Integer)
    date_completed = Column(Integer)
    simulation_data = Column(BLOB)
    result_data = Column(BLOB)


if __name__ == "__main__":
    db = create_engine('sqlite:///test_db.sqlite')
    db.drop_all()
