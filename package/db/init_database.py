#!/usr/bin/env python3

from sqlalchemy import create_engine

def init_db(db_uri):
    engine = create_engine(db_uri)

if __name__ == "__main__":
    