#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

protoc -I=$SCRIPT_DIR/proto/ --python_out=$SCRIPT_DIR/bin --js_out=import_style=commonjs,binary:$SCRIPT_DIR/../result-browser/src/proto $SCRIPT_DIR/proto/*.proto
