#!/usr/bin/env python3

from math import pi, sqrt

from ..cases.Case import Case
from ..grains.Grain import Grain
from ..fuel.Kno3Su import Kno3Su
from ..bin.simulation_output_pb2 import SimulationDataPoint, SimulationOutputs
from ..bin.simulation_parameters_pb2 import SimulationParameters

ATMOSPHERIC_PRESSURE_PA = 101325
ATMOSPHERIC_PRESSURE_PSI = 14.6959
R = 8.31446261815324  # m3*Pa*(K^-1)*(mol^-1)


def burn_proto(serialized_input):
    sp = SimulationParameters()
    sp.ParseFromString(serialized_input)

    grain = Grain(sp.grain_outer_diam_in,
                  sp.grain_inner_diam_in, sp.chamber_length_in)
    fuel = Kno3Su()
    throat_diameter = sp.throat_diam_in
    return burn(grain, fuel, throat_diameter)


def in3_to_m3(val):
    return val / pow(39.3701, 3.0)


def in2_to_m2(val):
    return val / pow(39.3701, 2.0)


def psi_to_pa(val):
    return (val * ATMOSPHERIC_PRESSURE_PA) / ATMOSPHERIC_PRESSURE_PSI


def pa_to_psi(val):
    return (val * ATMOSPHERIC_PRESSURE_PSI) / ATMOSPHERIC_PRESSURE_PA


def calculate_chamber_pressure(grain, chamber_mass_kg, fuel):
    # chamber_density = chamber_mass_kg / chamber_gap_volume
    chamber_gap_volume_in3 = grain.get_gap_volume()  # in3
    chamber_gap_volume_m3 = in3_to_m3(chamber_gap_volume_in3)
    chamber_density = chamber_mass_kg / chamber_gap_volume_m3  # kg/m3
    # density (rho) = mass (kg) / volume, in this case kg/m3

    # chamber pressure [density]*R*T+pAtm
    # Ideal Gas Law:
    # P = (rho)*R_specific*T
    R_specific = fuel.get_R_specific()
    new_chamber_pressure_pa = chamber_density * R_specific * \
        fuel.get_combustion_temperature() + ATMOSPHERIC_PRESSURE_PA
    return pa_to_psi(new_chamber_pressure_pa)


def burn(grain, fuel, nozzle_throat_diameter):
    chamber_pressure_psi = ATMOSPHERIC_PRESSURE_PSI
    chamber_mass_kg = 0  # kg
    time = 0.0
    delta_t = 0.1
    fuel_density = fuel.get_density()
    sim_output = SimulationOutputs()
    del sim_output.results[:]
    sim_over = False
    k = fuel.get_k()
    k_side = sqrt(k) * pow((2.0/(k+1)), (((k+1)/2.0)/(k-1)))
    sqrt_part = sqrt(R * fuel.get_combustion_temperature())

    if nozzle_throat_diameter:
        throat_area_in2 = pi * pow((nozzle_throat_diameter/2.0), 2.0)  # in2
        throat_area_m2 = in2_to_m2(throat_area_in2)  # convert in2 to m2
    else:
        throat_area_m2 = None

    while True:
        if grain.get_fuel_volume() <= 0 and abs(chamber_pressure_psi - ATMOSPHERIC_PRESSURE_PSI) <= 10:
            sim_over = True  # Simulation is over
        datapoint = SimulationDataPoint()
        datapoint.chamber_pressure_psi = chamber_pressure_psi
        datapoint.propellant_mass_remaining_kg = grain.get_fuel_mass(
            fuel_density)
        datapoint.chamber_volume_m3 = in3_to_m3(grain.get_gap_volume())
        sim_output.results.extend([datapoint, ])
        if sim_over:
            break
        burn_rate = fuel.get_burn_rate(
            chamber_pressure_psi)  # input PSI, get inches/sec
        m_burnt_kg = grain.step(delta_t * burn_rate, fuel_density)
        # print("Burn rate: {} mass_burnt {} pressure {}".format(
        #     burn_rate, m_burnt_kg, pressure))

        if not nozzle_throat_diameter:
            # Mass flow rate is infinite (test burn in air only)
            time += delta_t
            continue

        # Calculate mass ejected
        # Mass flow rate when choked:
        # https://www.grc.nasa.gov/www/k-12/airplane/mflchk.html
        chamber_pressure_pa = psi_to_pa(chamber_pressure_psi)
        delta_p = chamber_pressure_pa - ATMOSPHERIC_PRESSURE_PA

        mass_ejection_rate = (delta_p * throat_area_m2 *
                              k_side)/sqrt_part  # kg/sec
        mass_ejection_rate = max(mass_ejection_rate, 0.0)
        mass_ejected_kg = mass_ejection_rate * delta_t  # kg

        # Add mass burnt to stored mass
        chamber_mass_kg = chamber_mass_kg + m_burnt_kg - mass_ejected_kg
        chamber_mass_kg = max(chamber_mass_kg, 0.0)

        # print("pressure {} mass burnt {} ejected {}".format(pressure, m_burnt_kg, mass_ejected))
        chamber_pressure_psi = calculate_chamber_pressure(
            grain, chamber_mass_kg, fuel)

        time += delta_t
    return sim_output
