#!/usr/bin/env python3

from package.bin.simulation_parameters_pb2 import SimulationParameters
import argparse

def create_parser():
    parser = argparse.ArgumentParser(description='Create a simulation input binary')
    parser.add_argument('GRAIN_OUTER_DIAM', type=float, help="Outer diameter of the grain (in)")
    parser.add_argument('GRAIN_INNER_DIAM', type=float, help="Inner diameter of the grain (in)")
    parser.add_argument('CHAMBER_LENGTH', type=float, help="Inner diameter of the grain (in)")
    parser.add_argument('THROAT_DIAM', type=float, help="nozzle throat diameter (in)")
    return parser


def main(args):
    sim_params = SimulationParameters()
    
    sim_params.grain_outer_diam_in = args.GRAIN_OUTER_DIAM
    sim_params.grain_inner_diam_in = args.GRAIN_INNER_DIAM
    sim_params.chamber_length_in = args.CHAMBER_LENGTH
    sim_params.throat_diam_in = args.THROAT_DIAM

    return sim_params.SerializeToString()

if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args()
    print(main(args))
