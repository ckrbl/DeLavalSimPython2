#!/usr/bin/env python3

from package.burner.Burn import burn_proto

def burn_from_proto(proto):
    return burn_proto(proto)
