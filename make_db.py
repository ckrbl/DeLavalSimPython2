#!/usr/bin/env python3
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from package.db.Job import Job, Base
from package.bin.simulation_parameters_pb2 import SimulationParameters
import pdb

if __name__ == "__main__":
    sim_params = SimulationParameters()
    
    grain_outer_diam = 2
    sim_params.grain_outer_diam_in = float(grain_outer_diam)
    grain_inner_diam = 0.4
    sim_params.grain_inner_diam_in = float(grain_inner_diam)
    grain_length = 6
    sim_params.chamber_length_in = float(grain_length)
    throat_diam = 0.2
    sim_params.throat_diam_in = float(throat_diam)

    j = Job(simulation_data=sim_params.SerializeToString())
    engine = create_engine('sqlite:///test_db.sqlite')
    session = sessionmaker()
    session.configure(bind=engine)
    Base.metadata.create_all(engine)
    s = session()
    s.add(j)
    s.commit()

    pdb.set_trace()
    dir(sim_params)

    print()
