import React, { Component } from "react";

import axios from "axios";

import { Scatter } from "react-chartjs-2";

import { SimulationOutputs } from "../proto/simulation_output_pb";

export default class ResultComponent extends Component {
  state = { result: null };

  constructor(props) {
    super(props);
    console.log("in constructor " + props.uuid);
  }

  componentDidMount() {
    const url = `http://capstone:8080/getresult?param=`.concat(
      this.props.uuid
    );
    console.log(`url ${url}`);
    axios
      .get(url, {
        responseType: "blob", // Important
        headers: {
          "Content-Type": "application/octet-stream",
          Accept: "application/octet-stream",
        },
      })
      .then((res) => {
        let blob = new Blob([res.data], {
          type: "application/octet-stream",
        });
        blob.arrayBuffer().then((buffer) => {
          const result = SimulationOutputs.deserializeBinary(buffer).toObject();
          console.log(`result ${result}`);

          this.setState({ result });
        });
      })
      .catch(function (error) {
        console.error("BAD", error);
      });
  }

  renderGraph() {
    var pressures = this.state.result.resultsList.map(
      (x) => x.chamberPressurePsi
    );
    var time = [...pressures.keys()];
    time = time.map((x) => x / 10.0);

    var propellantMasses = this.state.result.resultsList.map(
      (x) => x.propellantMassRemainingKg * 1000
    );

    var xy = [5, 10, 15];
    const pressureData = {
      labels: time,
      datasets: [
        {
          label: "Chamber Pressure (psi)",
          data: pressures,
          fill: true,
          backgroundColor: "rgba(75,192,192,0.2)",
          borderColor: "rgba(75,192,192,1)",
        },
      ],
    };

    const massData = {
      labels: time,
      datasets: [
        {
          label: "Propellant mass (g)",
          data: propellantMasses,
          fill: true,
          borderColor: "#742774",
        },
      ],
    };
    return (
      <div>
        <div>
          <Scatter data={pressureData} />
        </div>
        <div>
          <Scatter data={massData} />
        </div>
      </div>
    );
  }

  render() {
    console.log(`test1 ${this.state.result}`);
    if (!this.state.result) {
      return <div>Downloading data</div>;
    }
    return this.renderGraph();
  }
}
