import React from 'react';
// import { Icon } from '@iconify/react';
// import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink } from 'react-router-dom';
// material
import { Grid, Button, Container, TextField, Stack, Typography } from '@mui/material';
// components
import Page from '../components/Page';
import axios from 'axios';

// import { BlogPostCard, BlogPostsSort, BlogPostsSearch } from '../components/_dashboard/blog';
//
// import POSTS from '../_mocks_/blog';

// import TextField from "@material-ui/core/TextField";
// import Button from "@material-ui/core/Button";

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const defaultValues = {
    grainOuterDiameter: 2.0,
    grainInnerDiameter: 0.5,
    chamberLength: 6.0,
    throatDiam: 0.4,
};


// ----------------------------------------------------------------------

export default class CreateSimulation extends React.Component {
    state = { formValues: defaultValues };

    constructor(props) {
        super(props);
        this.state = {
            formValues: defaultValues
        };
    }

    handleSubmit = (event) => {
        // event.preventDefault();
        // console.log(formValues);
    };

    handleInputChange = (e) => {
        const { name, value } = e.target;
        this.setState({ ...this.state.formValues, [name]: value, });
    };

    render() {
        return (
            <Page title="Create New simulation">
                <Container>
                    <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
                        <Typography variant="h4" gutterBottom>
                        </Typography>
                    </Stack>

                    <Typography variant="h6" gutterBottom>
                        Simulation Inputs
                    </Typography>
                    <form
                        method="post"
                        action="http://capstone:8080/submitnewsimulation"
                        onSubmit={this.handleSubmit}
                    >
                        <div>
                            <TextField
                                id="outerDiameter-input"
                                name="grainOuterDiameter"
                                label="Grain Outer Diameter (in)"
                                type="number"
                                defaultValue={this.state.formValues.grainOuterDiameter}
                                onChange={this.handleInputChange}
                            />
                        </div>
                        <div>
                            <TextField
                                id="innerDiameter-input"
                                name="grainInnerDiameter"
                                label="Grain Inner Diameter (in)"
                                type="number"
                                defaultValue={this.state.formValues.grainInnerDiameter}
                                onChange={this.handleInputChange}
                            />
                        </div>
                        <div>
                            <TextField
                                id="chamberLength-input"
                                name="chamberLength"
                                label="Chamber Length (in)"
                                type="number"
                                defaultValue={this.state.formValues.chamberLength}
                                onChange={this.handleInputChange}
                            />
                        </div>
                        <div>
                            <TextField
                                id="throatDiam-input"
                                name="throatDiam"
                                label="Throat Diameter (in)"
                                type="number"
                                defaultValue={this.state.formValues.throatDiam}
                                onChange={this.handleInputChange}
                            />
                        </div>
                        <Button variant="contained" color="primary" type="submit">
                            Submit
                        </Button>
                    </form>
                </Container>
            </Page>
        );

    }
}
