import React from 'react';
// import { Icon } from '@iconify/react';
// import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink } from 'react-router-dom';
// material
import { Grid, Button, Container, Stack, Typography } from '@mui/material';
// components
import Page from '../components/Page';
import axios from 'axios';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


// ----------------------------------------------------------------------

function formatDate(date) {
  let epochDate = new Date(0);
  epochDate.setUTCSeconds(date);
  return epochDate.toLocaleDateString("en-US");
}

function makeRows(simulations) {
  var a = [...simulations].map((row, idx) => (
    <TableRow
      key={idx}
      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
    >
      <TableCell component="th" scope="row">
        <RouterLink to={{
          pathname: "/dashboard/".concat(row.id),
          // hash: "#".concat(row.id)
        }} >{row.id}</RouterLink>
      </TableCell>
      <TableCell align="right">{
        formatDate(row.date)
      }</TableCell>
    </TableRow>
  ))
  return a;
}

export default class Landing extends React.Component {
  state = { simulations: [] };
  constructor(props) {
    super(props);
    this.state = {
      simulations: []
    };
  }

  componentDidMount() {
    const url = `http://capstone:8080/listsimulations`;
    axios
      .get(url, {
        responseType: "json", // Important
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      })
      .then((res) => {
        let simulations = res.data;
        this.setState({ simulations });
      })
      .catch(function (error) {
        console.error("BAD", error);
      });
  }

  render() {
    return (
      <Page title="DeLaval Nozzle Simulator">
        <Container>
          <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
            <Typography variant="h4" gutterBottom>
              DeLaval Nozzle Simulator
            </Typography>
            <Button
              variant="contained"
              component={RouterLink}
              to="/newsimulation"
            >
              Create New Simulation
            </Button>
          </Stack>

          <Typography variant="h6" gutterBottom>
            Recent Simulations
          </Typography>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
              <TableHead>
                <TableRow>
                  <TableCell>Simulation ID</TableCell>
                  <TableCell align="right">Date</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {makeRows(this.state.simulations)}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
      </Page>
    );
  }
}
