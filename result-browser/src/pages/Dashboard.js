import React from 'react';
// import { Icon } from '@iconify/react';
// import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink } from 'react-router-dom';
// import { useRouteMatch } from 'react-router-dom'
import { matchPath } from 'react-router-dom';

// material
import { Grid, Button, Container, Stack, Typography } from '@mui/material';
// components
import Page from '../components/Page';
import axios from 'axios';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { Scatter } from "react-chartjs-2";

import { SimulationOutputs } from "../proto/simulation_output_pb";



// ----------------------------------------------------------------------

export default class Dashboard extends React.Component {
    state = { result: null };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // console.log(window.location.host);

        // const url = window.location.protocol + '//' + window.location.host + `/getresult?param=` + this.props.name;
        const url = `http://capstone:8080/getresult?param=` + this.props.name;
        console.log(`url ${url}`);
        axios
            .get(url, {
                responseType: "blob", // Important
                headers: {
                    "Content-Type": "application/octet-stream",
                    Accept: "application/octet-stream",
                },
            })
            .then((res) => {
                let blob = new Blob([res.data], {
                    type: "application/octet-stream",
                });
                blob.arrayBuffer().then((buffer) => {
                    const result = SimulationOutputs.deserializeBinary(buffer).toObject();
                    console.log(`result ${result}`);

                    this.setState({ result });
                });
            })
            .catch(function (error) {
                console.error("BAD", error);
            });
    }

    renderGraph() {
        if (this.state.result == null) {
            return <></>;
        }
        var pressures = this.state.result.resultsList.map(
            (x) => x.chamberPressurePsi
        );
        var time = [...pressures.keys()];
        time = time.map((x) => x / 10.0);

        var propellantMasses = this.state.result.resultsList.map(
            (x) => x.propellantMassRemainingKg * 1000
        );

        var xy = [5, 10, 15];
        const pressureData = {
            labels: time,
            datasets: [
                {
                    label: "Chamber Pressure (psi)",
                    data: pressures,
                    fill: true,
                    backgroundColor: "rgba(75,192,192,0.2)",
                    borderColor: "rgba(75,192,192,1)",
                },
            ],
        };

        const massData = {
            labels: time,
            datasets: [
                {
                    label: "Propellant mass (g)",
                    data: propellantMasses,
                    fill: true,
                    borderColor: "#742774",
                },
            ],
        };
        return (
            <div>
                <div>
                    <Scatter data={pressureData} />
                </div>
                <div>
                    <Scatter data={massData} />
                </div>
            </div>
        );
    }

    render() {
        return (
            <>
                <Page title="Simulation Results">
                    <Container>
                        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
                            <Typography variant="h4" gutterBottom>
                                Simulation Results {this.props.name}
                            </Typography>
                        </Stack>
                        <Button
                            variant="contained"
                            // component={RouterLink}
                            href={"http://capstone:8080/getresult?param=".concat(this.props.name)}
                        >
                            Download Simulation
                        </Button>
                        {this.renderGraph()}
                    </Container>
                </Page>
            </>
        );
    }
}
