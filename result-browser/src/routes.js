import { Navigate, useParams, Routes, Route } from 'react-router-dom';

import CreateSimulation from './pages/CreateSimulation';
import Landing from './pages/Landing';
import NotFound from './pages/Page404';
import Dashboard from './pages/Dashboard';

function FnDashboard2() {
  let { name } = useParams();
  return <Dashboard name={name}></Dashboard>
}

function FnDashboard() {
  return (<Routes>
    <Route path=":name" element={<FnDashboard2 />} />
  </Routes>);
}

export default function Router() {
  return (
    <Routes>
      <Route exact path="/" element={<Landing />} />
      <Route exact path="/newsimulation" element={<CreateSimulation />} />
      <Route exact path="/dashboard" element={<Navigate to="/" />} />
      <Route path="/dashboard/*" element={<FnDashboard />} />
      <Route exact path="/404" element={<NotFound />} />
      <Route path="*" element={<Navigate to="/404" />} />
    </Routes >
  );
}
