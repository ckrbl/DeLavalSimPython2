import Router from './routes';
import logo from "./logo.svg";
import "./App.css";

import ResultComponent from "./components/ResultComponent";

import { ThemeProvider, createTheme } from "@mui/material/styles";

const themeLight = createTheme({
  palette: {
    background: {
      default: "#e4f0e2"
    }
  }
});

const themeDark = createTheme({
  palette: {
    background: {
      default: "#222222"
    },
    text: {
      primary: "#ffffff"
    }
  }
});


// import {
//   BrowserRouter as Router,
//   Routes,
//   Route,
//   useParams,
// } from "react-router-dom";
import {useState} from "react";

// export function ProductPage() {
//   const { id } = useParams();
//   return (
//     <div>
//       <p>This is the first page</p>
//       <p>The id was {id}</p>
//       <ResultComponent uuid={id} />
//     </div>
//   );
// }

function App() {
  // const { number } = useParams();
  // const [light, setLight] = useState(true);

  return (
    // <ThemeProvider theme={light ? themeLight : themeDark}>
      <Router />
    // </ThemeProvider>
  );
}

export default App;
