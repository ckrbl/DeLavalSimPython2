#!/usr/bin/env python3

from zmq import Context, PUSH, PULL
from threading import Event, Thread
from time import sleep
from sys import path
from package.db.Job import Job, Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from package.jobmanager.JobManager import JobManagerRoot, JobManagerWorkers, Worker
from uuid import uuid4
from signal import pause
from pdb import set_trace

path.insert(0, "package/bin")

from package.bin.simulation_parameters_pb2 import SimulationParameters
from package.bin.simulation_output_pb2 import SimulationOutputs
from package.bin.jobmanager_message_pb2 import JobManagerMessage
from package.bin.jobmanager_configuration_pb2 import JobManagerConfiguration, SimulationRunner, SimulationRoot


engine1 = create_engine('sqlite:///sqlite3.db', connect_args={'check_same_thread': False})
Session1 = sessionmaker(bind=engine1)
engine2 = create_engine('sqlite://', connect_args={'check_same_thread': False})  # creates temporary in-memory database
Session2 = sessionmaker()

def connection1():
    connection = engine1.connect()
    Base.metadata.create_all(engine1)
    yield connection
    connection.close()

def connection2():
    connection = engine2.connect()
    Base.metadata.create_all(engine2)
    yield connection
    connection.close()

def session1(connection1):
    connection1.begin()
    session = Session1(bind=connection1)
    yield session
    session.close()

def session2(connection2):
    transaction = connection2.begin()
    session = Session2(bind=connection2)
    yield session
    session.close()
    transaction.rollback()

ROOT_MANAGER_PORT = 5553
CHILD_MANAGER_PORT = 5555

def main():
    conn1 = engine1.connect()
    Base.metadata.create_all(engine1)
    conn1.begin()
    db_root = Session1()

    db_child = next(session2(next(connection2())))
    sim_params = SimulationParameters()
    sim_params.grain_outer_diam_in = 2.0
    sim_params.grain_inner_diam_in = 0.4
    sim_params.chamber_length_in = 0.6
    sim_params.throat_diam_in = 0.2

    jm_child_config = JobManagerConfiguration()
    jm_child_config.port_start = CHILD_MANAGER_PORT
    jm_child_config.sim_runner.num_workers = 2
    jm_child_config.sim_runner.root_server_pull_jobs = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT)
    jm_child_config.sim_runner.root_server_push_results = 'tcp://127.0.0.1:{}'.format(ROOT_MANAGER_PORT+1)

    jobs = db_root.query(Job).all()
    print("num jobs {}".format(len(jobs)))
    # set_trace()

    jm_root_config = JobManagerConfiguration()
    jm_root_config.port_start = ROOT_MANAGER_PORT
    _sr = SimulationRoot()
    jm_root_config.sim_root.CopyFrom(_sr)
    j_root = JobManagerRoot(jm_root_config, db_root)
    
    j_child1 = JobManagerWorkers(jm_child_config, db_child)

    j_child1.start()
    j_root.start()

    try:
        pause() # wait forever, stop when ctrl-c hit
    finally:
        j_root.stop()
        j_child1.stop()
        
        db_root.close()
        conn1.close()

if __name__ == "__main__":
    main()
