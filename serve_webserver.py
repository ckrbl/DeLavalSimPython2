#!/usr/bin/env python3

from os.path import join, dirname, abspath
from package.webserver.start import main

DB_FILE_PATH = abspath(join(dirname(__file__), "sqlite3.db"))

if __name__ == '__main__':
    main(DB_FILE_PATH)
