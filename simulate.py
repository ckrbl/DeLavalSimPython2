#!/usr/bin/env python3

from package.cases.Case import Case
from package.grains.Grain import Grain
from package.burner.Burn import burn
from package.fuel.Kno3Su import Kno3Su

def compare_float(a, b):
    return abs(a-b) <= 0.01

def main():
    # c = Case()
    # print("case_volume= {}".format(c.get_volume()))
    # g = Grain(2.54, 1, 15)
    # print("grain_volume= {}".format(g.get_volume()))
    # print("grain_surface_area= {}".format(g.get_burn_surface_area()))
    grain = Grain(1, 0.4, 8)
    fuel = Kno3Su()
    throat_diameter = 0.15
    sim_outputs = burn(grain, fuel, throat_diameter)
    time = len(sim_outputs.results)/10
    print("sim completed in {} {}".format(time, compare_float(time,3.9)))

if __name__ == "__main__":
    main()
