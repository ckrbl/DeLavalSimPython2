#!/usr/bin/env python3

from package.input.SimulationRead import main, create_parser
import sys

if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args()
    sys.stdout.buffer.write(main(args))
    # print(main(args))
