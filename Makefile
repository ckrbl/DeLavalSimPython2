init:
	pip install -r requirements.txt

test:
	py.test-3 -v package/tests package/webserver/tests

.PHONY: init test
