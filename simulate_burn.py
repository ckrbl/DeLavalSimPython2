#!/usr/bin/env python3

from package.bin.simulation_parameters_pb2 import SimulationParameters
from package.burner.Burn import burn_proto
import sys

from package.input.BurnProto import burn_from_proto

if __name__ == '__main__':
    in_proto = sys.stdin.buffer.read()
    sim_outputs = burn_from_proto(in_proto)
    print('Done! Burn took {} sec'.format(len(sim_outputs.results)/10))
